﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Converter
{
    class Word
    {
        private string[] _mek { get; set; }
        private string[] _tas { get; set; }
        private string[] _har { get; set; }
        private string[] _haz { get; set; }
        private string[] _tasHaz { get; set; }
        private string[] _harHaz { get; set; }
        private string[] _mil { get; set; }
        private string[] _tasMil { get; set; }
        private string[] _harMil { get; set; }
        public Dictionary<int,string[]> DicWord{ get; set; }
        
        public Word()
        {
            _mek = new string[10] { "", "մեկ", "երկու", "երեք", "չորս", "հինգ", "վեց", "յոթ", "ութ", "ինը" };
            _tas = new string[10] { "", "տաս", "քսան", "երեսուն", "քառասուն", "հիսուն", "վաթսուն", "յոթանասուն", "ութսուն", "ինըսուն" };
            _har = new string[10] { "", "հարյուր", "երկուհարյուր", "երեքհարյուր", "չորսհարյուր", "հինգհարյուր", "վեցհարյուր", "յոթհարյուր", "ութհարյուր", "ինըհարյուր" };
            _haz = new string[10] { "", "հազար", "երկուհազար", "երեքհազար", "չորսհազար", "հինգհազար", "վեցհազար", "յոթհազար", "ութհազար", "ինըհազար" };
            _tasHaz = new string[10] { "", _tas[1] + _haz[1], _tas[2] + _haz[2], _tas[3] + _haz[3], _tas[4] + _haz[4], _tas[5] + _haz[5], _tas[6] + _haz[6], _tas[7] + _haz[7], _tas[8] + _haz[8], _tas[9] + _haz[9], };
            _harHaz = new string[10] { "", _har[1] + _haz[1], _har[2] + _haz[2], _har[3] + _haz[3], _har[4] + _haz[4], _har[5] + _haz[5], _har[6] + _haz[6], _har[7] + _haz[7], _har[8] + _haz[8], _har[9] + _haz[9], };
            _mil = new string[10] { "", "միլիոն", _mek[2] + "միլիոն", _mek[3] + "միլիոն", _mek[4] + "միլիոն", _mek[5] + "միլիոն", _mek[6] + "միլիոն", _mek[7] + "միլիոն", _mek[8] + "միլիոն", _mek[9] + "միլիոն" };
            _tasMil = new string[10] { "", _tas[1] + _mil[1], _tas[2] + _mil[1], _tas[3] + _mil[1], _tas[4] + _mil[1], _tas[5] + _mil[1], _tas[6] + _mil[1], _tas[7] + _mil[1], _tas[8] + _mil[1], _tas[9] + _mil[1], };
            _harMil = new string[10] { "", _har[1]+_mil[1], _har[2] + _mil[1], _har[3] + _mil[1], _har[4] + _mil[1], _har[5] + _mil[1], _har[6] + _mil[1], _har[7] + _mil[1], _har[8] + _mil[1], _har[9] + _mil[1], };
            DicWord = new Dictionary<int, string[]>(10);
            DicWord.Add(1, _mek);
            DicWord.Add(2, _tas);
            DicWord.Add(3, _har);
            DicWord.Add(4, _haz);
            DicWord.Add(5, _tasHaz);
            DicWord.Add(6, _harHaz);
            DicWord.Add(7, _mil);
            DicWord.Add(8, _tasMil);
            DicWord.Add(9, _harMil);
        }
    }
}
